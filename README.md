# ActCulMad

# Idea General

Buscador de pisos de airbnb para acudir a las actividades culturales y de ocio propuestas por el ayuntamiento de Madrid que tengan cerca aparcamientos publicos.

Basándose en las actividades culturales y de ocio del ayuntamiento de Madrid, sacar las actividades del próximo mes, recomendar apartamentos de airbnb cerca
y, si es posible, mostrar aparcamientos públicos cercanos.

# Nombre del Producto

ActCulMad = Actividades Culturales Madrid.
Recomendador de Airbnb para ActCulMad. 

### Estrategia del DAaas

A diario sacar un informe con las actividades culturales del próximo mes, esto es de los próximos 30 días, con los pisos de airbnb y aparcamientos públicos mejor situados
para acudir a estas actividades.

Utilizar herramientas en la nube para facilitar el manejo de los datos.

### Arquitectura

Arquitectura Cloud basada en API + Google Cloud Storage + HIVE + Dataproc.

Usar APIs para leer datos publicos de actividades culturales y aparcamientos publicos del ayuntamiento de Madrid obteniendo los resultados en csv.
Ejecutarlo en un Cloud Function.

Insertar el dataset de airbnb en HIVE.

Colocar el resultado de los APIs y del dataset de airbnb en un segmento de Google Cloud.

Desde Google Storage coger los datos para crear 3 tablas de HIVE y realizar, no se cómo todavía, una query con un JOIN que reste las distancias entre cada actividad cultural
y los pisos de airbnb y los aparcamientos. (Hacer todo en 1 JOIN, 2 JOIN y fusionarlos???)

De la query final obtener las actividades culturales del próximo mes con los 5 apartamentos de airbnb y los 3 aparcamientos más cercanos.

El resultado de la query estara en Google Storage.

### Operating Model

El operador va a disparar el cloud function de actividades una vez al día para obtener las acividades culturales y de ocio del mes siguiente.
(El ayuntamiento de Madrid actualiza este API diariamente)
Esto disparara el cloud function y guardara el resultado en un directorio del segmento llamado "input/actividades/".
Elfichero se llamará actividades.csv.

El operador va a disparar el cloud function de aparcamientos una vez al año para obtener los aparcamientos municipales del año siguiente.
(El ayuntamiento de Madrid actualiza este API anualmente)
Esto disparara el cloud function y guardara el resultado en un directorio del segmento llamado "input/aparcamientos/".
Elfichero se llamará aparcamientos.csv.

En el segmento siempre habra un directorio llamado "input/airbnb/".

Seguir el estándar de levantar el Cluster solamente cuando quiera regenerar el listado de actividades del mes siguiente.

Una vez al día, levantar el CLUSTER a mano y enviar las tareas de:
 
-  Crear tablas de airbnb, actividades y aparcamientos (anual).
-  LOAD DATA INPATH 'gs://bd5dextra/input/actividades/actividades.csv' INTO TABLE actividades;
-  LOAD DATA INPATH 'gs://bd5dextra/input/airbnb/airbnb.csv' INTO TABLE airbnb;
-  LOAD DATA INPATH 'gs://bd5dextra/input/aparcamientos/aparcamientos.csv' INTO TABLE aparcamientos;
-  SELECT JOIN INTO DIRECTORY 'gs://output/results'

Apagar el cluster.

Montar una web con un link directo al Google Storage Segment Object, o algo similar, para visualizar los datos.

### Desarrollo

Código de las 2 Cloud Functions que uso

***CloudFunction carga_actividades***

import request

from google.cloud import storage

def store_file_in_bucket(file_name):

    storage_client = storage.Client('eastern-surface-264620')
    
    bucket = storage_client.get_bucket('bd5dextra')
    
    blob = bucket.blob('input/actividades/actividades.csv')
    
    blob.upload_from_filename(file_name)
	
def download_file_actividades(request):

    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:71.0) Gecko/20100101 Firefox/71.0'}
    
    response = requests.get("https://datos.madrid.es/egob/catalogo/206974-0-agenda-eventos-culturales-100.csv", headers=headers)
 
    open('/tmp/fichero.csv', 'wb').write(response.content)    
    
    store_file_in_bucket('/tmp/fichero.csv')
    
    return "OK"
	
***CloudFunction carga_aparcamientos***	

import requests

from google.cloud import storage

def store_file_in_bucket(file_name):

    storage_client = storage.Client('eastern-surface-264620')
    
    bucket = storage_client.get_bucket('bd5dextra')
    
    blob = bucket.blob('input/aparcamientos/aparcamientos.csv')
    
    blob.upload_from_filename(file_name)
	
def download_file_aparcamientos(request):

    headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:71.0) Gecko/20100101 Firefox/71.0'}
    
    response = requests.get("https://datos.madrid.es/egob/catalogo/202625-0-aparcamientos-publicos.csv", headers=headers)
 
    open('/tmp/fichero.csv', 'wb').write(response.content)    
    
    store_file_in_bucket('/tmp/fichero.csv')
    
    return "OK"
    
***HIVE***

Se escoge HIVE como estructura de almacenamiento porque los datos están estructurados y la Base de Datos Relacional HIVE se considera la mejor opción.

Se han generado las siguientes tareas para cargar los datos desde los csv a HIVE :

https://drive.google.com/file/d/1wl0dyJv2y3-x6oIbHkT-oW207s34HGqq/view?usp=sharing


### Diagrama

https://drive.google.com/file/d/19nQ2epeIGmwEDW9VP4PXI3o9bk4E1WhE/view?usp=sharing


## Información Adicional

#### Scraping de Actividades Culturales
https://colab.research.google.com/drive/1B7LNUHXkwC25pTJT1K-HJLqso3AY6dsn

#### Scraping de Aparcamientos
https://colab.research.google.com/drive/1hAsgtS3lJAWmmBMfNIL8BUiB9KcT-g8Z

#### Fichero con Pantallazos del cluster creado, del wordcount realizado y de un select sobre la tabla aparcamientos en HIVE
https://drive.google.com/file/d/1KL66McJji-TH9MvWnJZUvXbKYGyUrKI_/view?usp=sharing



